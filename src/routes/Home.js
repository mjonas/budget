import React, {Component} from 'react';
import {Link} from 'react-router';
import Layout from './Layout';

export default class IndexPage extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Layout title="Home">
				<div id="page-index" className="page">
					<div>
						<Link to="/items">Items</Link>
					</div>
					<div>
						<Link to="/incomes">Incomes</Link>
					</div>
					<div>
						<Link to="/users">Users</Link>
					</div>
				</div>
			</Layout>);
	}
}
