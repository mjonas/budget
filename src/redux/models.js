const models = [
  {
    singularName: 'user',
    pluralName: 'users',
		attributes: {
			id: {
				type: 'hidden'
			},
			username: {
				display: 'Username'
			},
			firstName: {
				display: 'First Name'
			},
			lastName: {
				display: 'Last Name'
			},
      city: {
        display: 'Citi'
      },
      state: {
        display: 'State',
        type: 'select',
				options: ['', 'AZ', 'NY', 'PA']
      },
			createdAt: {
				display: 'Create Date',
				type: 'readonly',
				format: function(value) {
					return (new Date(value)).toLocaleDateString();
				}
			},
		}
  }, {
    singularName: 'item',
    pluralName: 'items',
		attributes: {
			id: {
				type: 'hidden'
			},
			name: {
				display: 'Name'
			},
			description: {
				display: 'Description'
			},
			type: {
				display: 'Type',
				type: 'select',
				options: ['', 'Widget', 'Service', 'Other']
			},
			createdAt: {
				display: 'Create Date',
				type: 'readonly',
				format: function(value) {
					return (new Date(value)).toLocaleDateString();
				}
			},
		}
  }, {
    singularName: 'income',
    pluralName: 'incomes',
		attributes: {
			id: {
				type: 'hidden'
			},
			name: {
				display: 'Name'
			},
			amount: {
				display: 'Amount'
			},
			dayOfMonth: {
				display: 'Day of Month'
			},
      month: {
        type: 'group',
        options: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
      },
      year: {
        type: 'group',
        options: ['2019']
      },
			createdAt: {
				display: 'Create Date',
				type: 'readonly',
				format: function(value) {
					return (new Date(value)).toLocaleDateString();
				}
			},
		}
  },
];

export {models};
