import React, {Component} from 'react';
import {Router, Route, browserHistory, IndexRoute} from 'react-router';

import Home from './routes/Home';
import Sailboat from 'sailboat';
import NotFound from './routes/NotFound';
import { models } from './redux/models';

export default class App extends Component {
	constructor(props) {
		super(props);
	}
	render() {
		var sailboat = new Sailboat();
		console.log(sailboat, 'Sailboat');
		return (
		<Router history={browserHistory}>
			<Route path="/">
				<IndexRoute component={Home}/>
					{sailboat.renderRoutes(models)}
				<Route path="*" component={NotFound}/>
			</Route>
		</Router>);
	}
}
