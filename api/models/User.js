module.exports = {
	attributes: {
		username: {
			type: 'string',
			required: true
		},
		firstName: {
			type: 'string',
			required: false
		},
		lastName: {
			type: 'string',
			required: true
		},
		city: {
			type: 'string',
			required: false
		},
		state: {
			type: 'string',
			required: false
		}
	},
	getAll: function (callback) {
		this.find({}).exec(callback);
	},
	createItem: function (username, firstName, lastName, callback) {
		var	values = {
			username: username,
			firstName: firstName,
			lastName: lastName
		};
		this.create(values).fetch().exec(callback);
	},
	deleteAll: function (callback) {
		this.destroy({}).fetch().exec(callback);
	},
	updateItem: function (id, data, callback) {
		this.update({
			id: id
		}, data).fetch().exec(callback);
	},
	deleteItem: function (id, callback) {
		this.destroy({
			id: id
		}).fetch().exec(callback);
	}
};
