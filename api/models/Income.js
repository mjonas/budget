module.exports = {
	attributes: {
		name: {
			type: 'string',
			required: true
		},
		amount: {
			type: 'number',
			required: true
		},
		dayOfMonth: {
			type: 'number',
			required: true
		},
	},
	getAll: function (callback) {
		this.find({}).exec(callback);
	},
	createItem: function (name, description, type, callback) {
		var	values = {
			name: name,
			description: description,
			type: type
		};
		this.create(values).fetch().exec(callback);
	},
	deleteAll: function (callback) {
		this.destroy({}).fetch().exec(callback);
	},
	updateItem: function (id, data, callback) {
		this.update({
			id: id
		}, data).fetch().exec(callback);
	},
	deleteItem: function (id, callback) {
		this.destroy({
			id: id
		}).fetch().exec(callback);
	}
};
